import math
import numpy
from perevod import perevod
from TableManipPZK_4 import TableManipPZK_4


word='    ';
met=True;
x=100;
y=100;
z=200;
while met:
    g1=numpy.matrix([0, 0, 0, 0]);
    g2=numpy.matrix([2*math.pi, 205.87*math.pi/180, 205.87*math.pi/180, 205.87*math.pi/180]);
    t=g1-0.25*g2;
    tmin=t;
    toh=20;
    fl=False;
    bmin=4000;
    while not(fl):
        i=1;
        while (i<16):
            k=0;
            t=g1-0.25*g2;
            kod=perevod(i);
            while kod>0:
                if kod%10==1:
                    t[0,k]=g1[0,k]+0.5*g2[0,k];
                k=k+1;
                kod=kod//10;
           # print(t)
            b=TableManipPZK_4(x,y,z,t[0,0],t[0,1],t[0,2],t[0,3]);
            if (b<bmin):
                bmin=b;
                tmin=t;
                print(bmin);
            if (b<toh):
                fl=True;
                tmin=t;
            i=i+1;
        g1=tmin;
        g2=g2/2;
    print(tmin);
        
    tp=tmin;
    for j in range(0,3):
        if (abs(tp[0,j])>math.pi):
            tp[0,j]=tp[0,j]-tp[0,j]/abs(tp[0,j])*2*math.pi;
    print(tp);
    x=input(word);
    y=input(word);
    z=input(word);
    if ((x==1) and (y==2) and (z==3)):
        met=False;